const { testQuery } = require('../utils/sqs')

const index = async() => {
    try {
        //await receivedMessageSQS();
        await testQuery();
    } catch (error) {
        console.error(error);
    }
    process.exit();
};

index();