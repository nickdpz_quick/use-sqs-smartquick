require('dotenv').config();

const config = {
    dev: process.env.NODE_ENV !== 'production',
    port: process.env.PORT || 3000,
    database_name: process.env.NODE_ENV !== 'development' ? process.env.DB_NAME_PRODUCTION : process.env.DB_NAME_DEVELOPMENT,
    database_user: process.env.NODE_ENV !== 'development' ? process.env.DB_USER_PRODUCTION : process.env.DB_USER_DEVELOPMENT,
    database_port: process.env.NODE_ENV !== 'development' ? process.env.DB_PORT_PRODUCTION : process.env.DB_POST_DEVELOPMENT,
    database_host: process.env.NODE_ENV !== 'development' ? process.env.DB_HOST_PRODUCTION : process.env.DB_HOST_DEVELOPMENT,
    database_pass: process.env.NODE_ENV !== 'development' ? process.env.DB_PASS_PRODUCTION : process.env.DB_PASS_DEVELOPMENT
};

module.exports = { config };