'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class logs_sqs extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    logs_sqs.init({
        data_received: DataTypes.JSON,
        data_send: DataTypes.JSON,
        message_id: DataTypes.STRING,
        company: DataTypes.STRING,
        status: DataTypes.INTEGER,
    }, {
        sequelize,
        modelName: 'logs_sqs',
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });
    return logs_sqs;
};