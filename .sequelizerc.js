const path = require('path');
require('dotenv').config();
const config = {
    'config': path.resolve('config', 'database.js'),
    'models-path': path.resolve('database', 'models'),
    'seeders-path': path.resolve('database', 'seeders'),
    'migrations-path': path.resolve('database', 'migrations'),
    'debug': (process.env.NODE_ENV === 'development')
};

module.exports = config;