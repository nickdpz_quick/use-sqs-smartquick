const { receivedMessageSQS } = require('../utils/sqs')
const cron = require('node-cron');


const index = async() => {
    try {
        await receivedMessageSQS();
    } catch (error) {
        console.error(error);
    }
};

const task = cron.schedule('* * * * *', index, { scheduled: true });
module.exports = task;