const axios = require('axios');
const AWS = require('aws-sdk');
const { logs_sqs } = require('../database/models');


AWS.config.update({
    region: process.env.REGION_AWS,
    accessKeyId: process.env.ACCESS_KEY_ID_AWS,
    secretAccessKey: process.env.SECRET_ACCESS_KEY_AWS
});

const sqs = new AWS.SQS({ apiVersion: '2012-11-05' });

const setMessage = async(message) => {
    try {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': message.jwt
        };
        const data_prod = message.data;
        const baseUrl = message.url;
        console.log('***************** baseURL', baseUrl, '*****************');
        const { status } = await axios.post(`${baseUrl}`, data_prod, {
            headers: headers
        });
        const deleteMessage = await deleteMessageQueue(message.ReceiptHandle);
        console.log('----------------- Success Message  processed ', JSON.stringify({...message, data: 'not show' }), ' Delete response ', deleteMessage, '-----------------');

        await logs_sqs.update({ status }, { where: { message_id: message.id } })
        console.log('***************** update', message.id, '*****************');
    } catch (error) {
        console.error(error);
    }

};

const deleteMessageQueue = (ReceiptHandle) => {
    return new Promise((resolve, reject) => {
        sqs.deleteMessage({
            QueueUrl: process.env.MAIN_QUEUE_URL,
            ReceiptHandle
        }, (err, data) => {
            if (err) {
                reject(err);
                console.error('----------------- Error Delete Message', JSON.stringify(err), '-----------------');
            } else {
                resolve(JSON.stringify(data.ResponseMetadata));
            }
        });
    });
};

const receiveMessageQueue = () => {
    return new Promise((resolve, reject) => {
        sqs.receiveMessage({
            AttributeNames: [
                'SentTimestamp'
            ],
            MaxNumberOfMessages: 10,
            MessageAttributeNames: [
                'All'
            ],
            QueueUrl: process.env.MAIN_QUEUE_URL,
            WaitTimeSeconds: 20 //tiempo de espera de mensajes nuevos
        }, (err, data) => {
            if (err) {
                reject(err);
            } else if (data.Messages) {
                let messages = data.Messages.map((item) => ({...JSON.parse(item.MessageAttributes.data.StringValue), ReceiptHandle: item.ReceiptHandle, id: item.MessageId }));
                resolve(messages);
            }
        });
    });
};

const receivedMessageSQS = async() => {
    let messages;
    try {
        messages = await receiveMessageQueue();
    } catch (error) {
        console.error(error);
    }
    for (const message of messages) {
        await setMessage(message);
    }
};

const testQuery = async() => {
    try {
        let data = await logs_sqs.findOne()
        console.log(JSON.parse(JSON.stringify(data)))
    } catch (error) {
        console.error(error)
    }
}

module.exports = { receivedMessageSQS, testQuery }